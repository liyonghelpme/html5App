/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.ionicframework.admob2366364;

import android.os.Bundle;
import org.apache.cordova.*;
import android.util.Log;

import com.phkg.b.BManager;
import com.phkg.b.BannerView;
import com.phkg.b.MyBMDevListner;
import android.widget.LinearLayout;

public class CordovaApp extends CordovaActivity implements  MyBMDevListner
{
    public static String Tag = "Ad APp";
    public  static CordovaApp app;
    
    BannerView bannerView;

    @Override
    public void onCreate(Bundle savedInstanceState)
    { 
        app = this;
        super.onCreate(savedInstanceState);
        super.init();
        // Set by <content src="index.html" /> in config.xml
        loadUrl(launchUrl);
        
        Log.d(Tag, " start App ++++++++++++++++++++++++++****************");
        
        /*
        initAd();
         Log.d(Tag, "start   Ad");
        BManager.showTopBanner(CordovaApp.this,
            BManager.CENTER_BOTTOM, BManager.MODE_APPOUT, //"f946b3d4086249a6968aabec7c752027",
            "e2fbb99bf33e43648a0f80622833b1a2",
            "bm-appchina");
        */
    }

    private void initAd() {
      bannerView = new BannerView(this);
      LinearLayout adViewLayout = new LinearLayout(CordovaApp.this);
      LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

      appView.addView(adViewLayout);
      adViewLayout.addView(bannerView);
    
      BManager.setBMListner(this);
  
    }
    @Override
  public void onInstall(int id) {
    Log.d(Tag, "++++++++++++ onInstall  id :"+id);
  }

  /* 此方法 只在banner条展示的时候调用
   * @see com.fpkg.b.BMListner#onShowBanner()
   */
  @Override
  public void onShowBanner() {
    Log.d(Tag, "++++++++++++ banner  is  show");
  }
}
