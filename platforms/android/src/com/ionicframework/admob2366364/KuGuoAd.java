package com.ionicframework.admob2366364;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.apache.cordova.CallbackContext;

import com.phkg.b.BManager;
import com.phkg.b.BannerView;
import com.phkg.b.MyBMDevListner;
import android.widget.LinearLayout;


import android.util.Log;
public class KuGuoAd extends CordovaPlugin implements  MyBMDevListner{
	public static String Tag = "KuGuoAd";
  


	@Override
  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    	super.initialize(cordova, webView);
    	
    	Log.d(Tag, "++++++++ inistal kuguo");

	}

  private BannerView bannerView = null;

	@Override
  public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
  		Log.d(Tag, "+++++++++++ execute kuguo");

      cordova.getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
              if(bannerView == null) {
                  bannerView = new BannerView(CordovaApp.app);
                  LinearLayout adViewLayout = new LinearLayout(CordovaApp.app);
                  LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

                  webView.addView(adViewLayout);
                  adViewLayout.addView(bannerView);
                
                  BManager.setBMListner(KuGuoAd.this);
              }

              Log.d(Tag, "start   Ad +++ show View");
                BManager.showTopBanner(CordovaApp.app,
                    BManager.CENTER_BOTTOM, BManager.MODE_APPOUT, //"f946b3d4086249a6968aabec7c752027",
                    "e2fbb99bf33e43648a0f80622833b1a2",
                    "bm-appchina");

        }
      });

      
      return true;
	}  


   @Override
  public void onInstall(int id) {
    Log.d(Tag, "++++++++++++ onInstall  id :"+id);
  }

  /* 此方法 只在banner条展示的时候调用
   * @see com.fpkg.b.BMListner#onShowBanner()
   */
  @Override
  public void onShowBanner() {
    Log.d(Tag, "++++++++++++ banner  is  show");
  }

}