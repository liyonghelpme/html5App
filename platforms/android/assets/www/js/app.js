// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

angular.module('app', ['ionic', 'ngSanitize', 'app.controllers', 'app.services', "ngCordova"])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    //console.useConsole(false);
    console.log("start app now ready");
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    if(window.cordova) {
     console.log("cordova is "+cordova);
    }
    if(window.plugins && window.plugins.AdMob) {
      console.log("load admob start");
      var admob_key = device.platform == "Android" ? "ca-app-pub-3005563968785773/4805648644" : "ca-app-pub-3005563968785773/9375449045";
      var admob = window.plugins.AdMob;
      admob.createBannerView(
      {
        adId: admob_key,
        position : AdMob.AD_POSITION.BOTTOM_CENTER,
        autoShow : true,
      },
      function() {
        console.log("success load admob");
      }
      );

    }else {
     console.log("No plugins admob");
    }

  });
})

.config(function($stateProvider, $urlRouterProvider) {

  console.log("configure state");

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
    .state('questiongroup', {
      url: '/home/:groupId',
      templateUrl: 'templates/questiongroup.html',
      controller : 'QuestionGroupCtrl',
    })
    
    .state('home', {
      url: '/home',
      //abstract: true,
      templateUrl: 'templates/home.html',
      controller: 'HomeCtrl',
    })
    
    .state('question', {
      url: '/home/:groupId/:questId',
      //abstract : true,
      templateUrl: 'templates/question.html',
      controller: 'QuestionCtrl',
    })

    .state('test', {
      url : '/test',
      templateUrl : 'templates/test.html',
    })
    ;

  // if none of the above states are matched, use this as the fallback
  
  $urlRouterProvider.otherwise('/home');
  

});