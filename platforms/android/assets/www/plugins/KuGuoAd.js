cordova.define("com.liyong.KuGuo", function(require, exports, module) {
	var admob = {};
	function success() {
		console.log("success");
	}
	function fail() {
		console.log("fail");
	}
	admob.CreateBanner = function() {
		cordova.exec(success, fail, "KuGuoAd", "CreateBanner", []);
	};

	if (typeof module !== 'undefined') {
	  // Export admob
	  module.exports = admob;
	}

	window.KuGuo = admob;
});